package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

            int row = countRows(inputNumbers);
            int column = 2 * row - 1;

            try {

                int[][] buildPyramid = new int[row][column];

                Collections.sort(inputNumbers);

                Queue<Integer> q = new LinkedList<>(inputNumbers);

                int startPlace = (buildPyramid[0].length) / 2;

                for (int i = 0; i < buildPyramid.length; i++) {

                    try {

                        int firstElement = startPlace;

                        for (int j = 0; j <= i; j++) {
                            buildPyramid[i][firstElement] = q.remove();
                            firstElement += 2;
                        }
                        startPlace--;
                    }catch (NoSuchElementException e){
                        throw new CannotBuildPyramidException();
                    }
                }
            return buildPyramid;

            } catch (NullPointerException e) {
                throw new CannotBuildPyramidException();
            }catch (NegativeArraySizeException e){
                throw new CannotBuildPyramidException();
            }
        }

    private int countRows(List<Integer> input) {

            double result = (Math.sqrt(1 + 8 * input.size()) - 1) / 2;
            return (int) Math.ceil(result);

    }

}

