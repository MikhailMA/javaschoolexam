package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        try {
            List list1 = new ArrayList(x);
            List list2 = new ArrayList(y);

            List list3 = new ArrayList<>();
            List list4 = new ArrayList<>(list1);

            for (int i = 0; i < list2.size(); i++) {
                for (int j = 0; j < list1.size(); j++) {
                    if (list2.get(i).equals(list1.get(j))) {
                        list3.add(list2.get(i));
                        list1.remove(j);
                        break;
                    } else {
                        break;
                    }
                }
            }

            if (list4.equals(list3)) {
                return true;
            } else {
                return false;
            }
        }
        catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
    }
}
