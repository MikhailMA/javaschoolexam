package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    public static final Map<String, Integer> MAIN_MATH_OPERATIONS;

    static {
        MAIN_MATH_OPERATIONS = new HashMap<String, Integer>();
        MAIN_MATH_OPERATIONS.put("*", 1);
        MAIN_MATH_OPERATIONS.put("/", 1);
        MAIN_MATH_OPERATIONS.put("+", 2);
        MAIN_MATH_OPERATIONS.put("-", 2);
    }

    public static String sortingStation(String statement, Map<String, Integer> operations, String leftBracket,
                                        String rightBracket) {
        if (statement == null || statement.length() == 0)
            throw new IllegalStateException("Expression isn't specified.");
        if (operations == null || operations.isEmpty())
            throw new IllegalStateException("Operations aren't specified.");

        List<String> out = new ArrayList<String>();

        Stack<String> stack = new Stack<String>();

        statement = statement.replace(" ", "");
        statement = statement.replace("(-", "(0-");

        Set<String> operationSymbols = new HashSet<String>(operations.keySet());
        operationSymbols.add(leftBracket);
        operationSymbols.add(rightBracket);

        int index = 0;

        boolean findNext = true;
        while (findNext) {
            int nextOperationIndex = statement.length();
            String nextOperation = "";
            for (String operation : operationSymbols) {
                int i = statement.indexOf(operation, index);
                if (i >= 0 && i < nextOperationIndex) {
                    nextOperation = operation;
                    nextOperationIndex = i;
                }
            }

            if (nextOperationIndex == statement.length()) {
                findNext = false;
            } else {
                if (index != nextOperationIndex) {
                    out.add(statement.substring(index, nextOperationIndex));
                }
                if (nextOperation.equals(leftBracket)) {
                    stack.push(nextOperation);
                }
                else if (nextOperation.equals(rightBracket)) {
                    while (!stack.peek().equals(leftBracket)) {
                        out.add(stack.pop());
                        if (stack.empty()) {
                            throw new IllegalArgumentException("Unmatched brackets");
                        }
                    }
                    stack.pop();
                }
                else {
                    while (!stack.empty() && !stack.peek().equals(leftBracket) &&
                            (operations.get(nextOperation) >= operations.get(stack.peek()))) {
                        out.add(stack.pop());
                    }
                    stack.push(nextOperation);
                }
                index = nextOperationIndex + nextOperation.length();
            }
        }

        if (index != statement.length()) {
            out.add(statement.substring(index));
        }
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        StringBuffer result = new StringBuffer();
        if (!out.isEmpty())
            result.append(out.remove(0));
        while (!out.isEmpty())
            result.append(" ").append(out.remove(0));

        return result.toString();
    }

    public static String sortingStation(String statement, Map<String, Integer> operations) {
        return sortingStation(statement, operations, "(", ")");
    }

    public static double calculateExpression(String statement) {
        String rpn = sortingStation(statement, MAIN_MATH_OPERATIONS);
        StringTokenizer tokenizer = new StringTokenizer(rpn, " ");
        Stack<Double> stack = new Stack<Double>();

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (!MAIN_MATH_OPERATIONS.keySet().contains(token)) {
                stack.push(new Double(token));
            } else {
                Double operand2 = stack.pop();
                Double operand1 = stack.empty() ? 0.0 : stack.pop();

                if (token.equals("*")) {
                    stack.push(operand1 * operand2);
                } else if (token.equals("/")) {
                    stack.push(operand1 / operand2);
                } else if (token.equals("+")) {
                    stack.push(operand1 + operand2);
                } else if (token.equals("-")) {
                    stack.push(operand1 - operand2);

                }

            }
        }
        if (stack.size() != 1)
            throw new IllegalArgumentException("Expression syntax error.");
        return stack.pop();
    }

    public boolean equalLeftBracketRightBracket (String statement) {
        int countRightBracket = 0;
        int countLeftBracket = 0;

        for (char element : statement.toCharArray()) {
            if (element == '(') {
                countLeftBracket++;
            }
            if (element == ')') {
                countRightBracket++;
            }
        }
        if (countLeftBracket == countRightBracket) {
            return true;
        } else {
            return false;
        }
    }

    public String evaluate(String statement) {

        if(statement == null || statement.length() == 0) {
            return null;
        } else if((statement.contains("..")) ||(statement.contains("++"))||(statement.contains("--"))||
                (statement.contains("**"))||(statement.contains("//")) ||(statement.contains(","))){
            return null;
        } else if(!equalLeftBracketRightBracket(statement)){
            return null;
        }else {
            if (String.valueOf(calculateExpression(statement)).contains(".0")){
                int result = (int) calculateExpression(statement);
                return String.valueOf(result);
            } else if (Double.isInfinite(calculateExpression(statement))) {
                return null;
            } else {
                return String.valueOf(calculateExpression(statement));
            }
        }
    }

}
